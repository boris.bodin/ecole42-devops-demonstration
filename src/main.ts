import {enableProdMode, importProvidersFrom} from '@angular/core';
import {bootstrapApplication} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {AppRoutingRoutes} from '@core/app-routing.routes';
import {AppComponent} from '@core/components/app.component';
import {environment} from '@environment';

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [importProvidersFrom(RouterModule.forRoot(AppRoutingRoutes), BrowserAnimationsModule)]
}).catch((err) => console.error(err));
