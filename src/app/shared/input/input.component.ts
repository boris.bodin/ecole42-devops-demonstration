import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatInputModule} from '@angular/material/input';

@Component({
  selector: 'app-input',
  standalone: true,
  imports: [
    MatInputModule
  ],
  template: `
    <mat-form-field appearance="outline">
      <mat-label>{{ label }}</mat-label>
      <input matInput #input [name]="name" [type]="type" [value]="value" (change)="valueChange.emit(input.value)" />
    </mat-form-field>
  `,
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
    `
  ]
})
export class InputComponent {
  @Input() name: string = '';
  @Input() label: string = '';
  @Input() type: string = 'text';
  @Input() value: string = '';
  @Output() valueChange = new EventEmitter<any>();
}
