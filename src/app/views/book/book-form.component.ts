import {CommonModule} from '@angular/common';
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {InputComponent} from '@shared/input/input.component';
import {Book} from './book';

@Component({
  selector: 'app-book-form',
  standalone: true,
  imports: [FormsModule, CommonModule, InputComponent, MatButtonModule],
  template: `
    <h1>Book Form
      <ng-container *ngIf="book?.id">&nbsp;:&nbsp;{{ book?.id}}</ng-container>
    </h1>
    <form *ngIf="book" class="row" (submit)="onSubmit()">
      <app-input class="col" name="title" [(value)]="book.title" label="Title"></app-input>
      <app-input class="col" name="author" [(value)]="book.author" label="Author"></app-input>
      <app-input class="col" name="description" [(value)]="book.description" label="Description"></app-input>
      <div class="col">
        <button mat-raised-button color="primary" type="submit">Add Book</button>
        <button mat-raised-button color="warn" type="button" (click)="onCancel()">Cancel</button>
      </div>
    </form>
  `,
  styles: [
    `
      :host {
        display: block;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 5px;
        box-shadow: 0 0 5px #ccc;
        margin: 20px;
      }

      .row {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
      }

      .col {
        flex: 1;
        margin-right: 10px;
        justify-content: center;
        display: flex;
        flex-direction: row;
      }

      app-input {
        margin-bottom: 10px;
        width: 100%;
      }

      button {
        margin-right: 10px;
      }
    `
  ]
})
export class BookFormComponent {
  @Input() book: Book | undefined;
  @Output() save = new EventEmitter<Book>();
  @Output() cancel = new EventEmitter<void>();

  onSubmit() {
    this.save.emit(this.book);
  }

  onCancel() {
    this.cancel.emit();
  }
}
