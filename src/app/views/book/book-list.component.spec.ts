import {Given, Then, When} from '@aegis-techno/ngx-testing-tools';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Book} from '@views/book/book';
import {BookService} from '@views/book/book.service';
import {BookListComponent} from './book-list.component';

describe('BookListComponent', () => {
  let fixture: ComponentFixture<BookListComponent>;
  let component: BookListComponent;
  let books: Array<Book> = [];

  Given(() => {
    books = [];
  });

  When(async () => {

    await TestBed.configureTestingModule({
      imports: [BookListComponent, NoopAnimationsModule]
    }).compileComponents();
    fixture = TestBed.createComponent(BookListComponent);
    component = fixture.componentInstance;

    const bookService = TestBed.inject(BookService);
    bookService.booksSubject.next(books);

    fixture.detectChanges();
    await fixture.whenStable();
  });

  Then('should be created', () => {
    expect(component).toBeTruthy();
  });

  describe('given books', () => {
    Given(() => {
      books = [
        Object.assign(new Book, {id: 1, title: 'title', author: 'author'}),
        Object.assign(new Book, {id: 2, title: 'title', author: 'author'})
      ];
    });

    Then('should display books table', () => {
      const bookTable = fixture.nativeElement.querySelector('app-book-table');
      expect(bookTable).toBeTruthy();
    });

    Then('should display good count of row', () => {
      const bookTable = fixture.nativeElement.querySelector('app-book-table');
      const rows = bookTable.querySelectorAll('mat-row');
      expect(rows.length).toEqual(books.length);
    });

    describe('when click on edit', () => {
      When(() => {
        const bookTable = fixture.nativeElement.querySelector('app-book-table');
        const editButton = bookTable.querySelector('button');
        editButton.click();
        fixture.detectChanges();
      });

      Then('should display value in book form', () => {
        const bookForm = fixture.nativeElement.querySelector('app-book-form');
        const titleInput = bookForm.querySelector('input[name="title"]');
        expect(titleInput.value).toEqual(books[0]!.title);
      });

      describe('when click on save', () => {
        When(() => {
          const saveBtn = fixture.nativeElement.querySelector('button[type="submit"]');
          saveBtn.click();
          fixture.detectChanges();
        });

        Then('should reset form', () => {
          const bookForm = fixture.nativeElement.querySelector('app-book-form');
          const titleInput = bookForm.querySelector('input[name="title"]');
          expect(titleInput.value).toEqual('');
        });
      });
    });

  });

  describe('onAdd', () => {
    When(() => {
      const saveBtn = fixture.nativeElement.querySelector('button[type="submit"]');
      saveBtn.click();
      fixture.detectChanges();
    });

    Then(() => {
      const bookTable = fixture.nativeElement.querySelector('app-book-table');
      const rows = bookTable.querySelectorAll('mat-row');
      expect(rows.length).toEqual(1);
    });
  });

  describe('onCancel', () => {
    When(() => {
      const cancelBtn = fixture.nativeElement.querySelector('button[type="button"]');
      cancelBtn.click();
      fixture.detectChanges();
    });

    Then(() => {
      const bookTable = fixture.nativeElement.querySelector('app-book-table');
      const rows = bookTable.querySelectorAll('mat-row');
      expect(rows.length).toEqual(0);
    });
  });

});
