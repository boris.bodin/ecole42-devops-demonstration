import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Book} from './book';

@Injectable({providedIn: 'root'})
export class BookService {
  booksSubject: BehaviorSubject<Array<Book>> = new BehaviorSubject<Array<Book>>([]);
  BOOK_ID = 0;

  constructor() {
    this.saveBook(Object.assign(
      new Book(),
      {title: 'Lord of the Rings', author: 'J.R.R. Tolkien', description: 'History of a ring'}
    ));
    this.saveBook(Object.assign(
      new Book(),
      {title: 'The Hobbit', author: 'J.R.R. Tolkien', description: 'History of a hobbit'}
    ));
    this.saveBook(Object.assign(
      new Book(),
      {title: 'The Silmarillion', author: 'J.R.R. Tolkien', description: 'History of the world'}
    ));
    this.saveBook(Object.assign(
      new Book(),
      {title: 'Harry Potter', author: 'J.K. Rowling', description: 'History of a wizard'}
    ));
  }

  getBooks() {
    return this.booksSubject.asObservable();
  }

  saveBook(book: Book) {
    const books = this.booksSubject.getValue();
    if (!book.id) {
      book.id = ++this.BOOK_ID;
      books.push(book);
    } else {
      let oldBook = books.find((b) => b.id === book.id);
      if (oldBook) {
        Object.assign(oldBook, book);
      }
    }
    this.booksSubject.next([...books]);
  }
}
