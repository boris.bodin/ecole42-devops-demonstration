import {CommonModule} from '@angular/common';
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {Book} from './book';

@Component({
  selector: 'app-book-table',
  standalone: true,
  imports: [CommonModule, MatTableModule, MatButtonModule],
  template: `
    <h1>Books</h1>
    <div class="scroll-content">

      <mat-table [dataSource]="books">
        <ng-container matColumnDef="id">
          <mat-header-cell *matHeaderCellDef>#</mat-header-cell>
          <mat-cell *matCellDef="let book">{{ book.id }}</mat-cell>
        </ng-container>
        <ng-container matColumnDef="title">
          <mat-header-cell *matHeaderCellDef>Title</mat-header-cell>
          <mat-cell *matCellDef="let book">{{ book.title }}</mat-cell>
        </ng-container>
        <ng-container matColumnDef="author">
          <mat-header-cell *matHeaderCellDef>Author</mat-header-cell>
          <mat-cell *matCellDef="let book">{{ book.author }}</mat-cell>
        </ng-container>
        <ng-container matColumnDef="description">
          <mat-header-cell *matHeaderCellDef>Description</mat-header-cell>
          <mat-cell *matCellDef="let book">{{ book.description }}</mat-cell>
        </ng-container>
        <ng-container matColumnDef="actions">
          <mat-header-cell *matHeaderCellDef>Actions</mat-header-cell>
          <mat-cell *matCellDef="let book">
            <button mat-raised-button color="primary" type="button" (click)="edit.emit(book)">Edit</button>
          </mat-cell>
        </ng-container>
        <mat-header-row *matHeaderRowDef="['id','title', 'author', 'description', 'actions']; sticky: true"></mat-header-row>
        <mat-row *matRowDef="let book; columns: ['id','title', 'author', 'description', 'actions']"></mat-row>
      </mat-table>
    </div>
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 5px;
        box-shadow: 0 0 5px #ccc;
        margin: 20px;
        flex: 1;

        overflow: hidden;
      }

      .scroll-content {
        overflow: auto;
      }

    `
  ]
})
export class BookTableComponent {
  @Input() books: Array<Book> = [];
  @Output() edit = new EventEmitter<Book>();
}
